const express = require("express");
const router = express.Router();

const timer = (req, res, next) => {
  console.log("TIME : ", Date.now());
  next();
};

router.use(timer);

router.get("/router", (req, res) => {
  res.send("Hello dari router");
});

router.get("/products", (req, res) => {
  res.json(["apple", "Samsung", "One Plus"]);
});

router.get("/sengajaerror", (req, res) => {
  iniError; //ini penyebab error!;
});

router.get("/orders", (req, res) => {
  res.json([
    { id: 1, paid: false, user_id: 1 },
    { id: 2, paid: false, user_id: 2 },
  ]);
});

module.exports = router;
